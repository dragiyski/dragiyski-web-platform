(function () {
    'use strict';

    const vm = require('vm');
    const Implementation = require('@dragiyski/collection/implementation');
    const JavaScript = require('@dragiyski/javascript');

    const symbols = {
        type: Symbol('type'),
        scriptId: Symbol('scriptId'),
        interfaceFunctionMap: Symbol('interfaceFunctionMap')
    };

    class Platform extends Implementation {
        constructor(options = {}) {
            options = { ...options };
            super();
            if (options.type != null) {
                if (Array.isArray(options.type) || options.type === 'object' && Symbol.iterator in options.type) {
                    options.type = [...options.type];
                } else if (typeof options.type === 'string') {
                    options.tyoe = [options.type];
                } else {
                    throw new TypeError(`Expected option "type" to be string, [object Array] or [object Iterable], got ${JavaScript.getType(options.type)}`);
                }
            } else {
                options.type = ['Window'];
            }
            if (options.global != null) {
                if (!JavaScript.canStoreProperties(options.global)) {
                    throw new TypeError('Expected option "global" to be an object, if specified');
                }
            } else {
                options.global = globalThis;
            }
            Object.defineProperty(this, 'global', {
                value: options.global
            });
            if (!options.type.every(type => typeof type === 'string')) {
                throw new TypeError(`Expected option "type" to contain only strings`);
            }
            this[symbols.type] = Object.create(null);
            options.type.forEach(type => {
                this[symbols.type][type] = true;
            });
            this[symbols.interfaceFunctionMap] = new WeakMap();
            const agent = Object.create(null);
            this.setImplementation(this, agent);
            Object.defineProperties(this, {
                primordials: {
                    value: this._createPrimordials()
                }
            });
        }

        /**
         * Makes an interface function suitable for exposing it for public usage or assining it to public interface.
         *
         * The function will automatically transform any argument and this to an implementation for this platform. The function will also retrieve the
         * interface from the return value if the return value has interface.
         *
         * @param implementation
         * @param options
         * @param {string} options.filename Option passed to vm.compileFunction.
         * @param {number} options.lineOffset Option passed to vm.compileFunction.
         * @param {number} options.columnOffset Option passed to vm.compileFunction.
         * @param {function|Iterable<function>} options.before A function(s) to execute before implementation function is called.
         * @param {function|Iterable<function>} options.after A function(s) to execute after implementation function is called.
         * @param {function|Iterable<function>} options.onCatch A function(s) to execute if the implementation throws exception (those can be used to map one exception to another).
         * @param {function|Iterable<function>} options.onFinally A function(s) to execute if the implementation before the function exits. This is always executed, even if the implementation throws. It is highly advisable not to throw in those interceptors (if exception is already thrown it will be overridden). This is essentially "finally" clause code.
         * @param {object} options.entry Additional data for the entry object.
         * @param {string} options.name The name of the function. It must be empty string/null/undefined or valid JavaScript identifier.
         * @returns {function}
         */
        function(implementation, options = {}) {
            options = { ...options };
            const factoryOptions = {
                ...options.entry
            };
            const factoryArgs = [this, factoryOptions, implementation];
            let flags = 0;
            for (const interceptor of Object.keys(interceptorFlag)) {
                if (options[interceptor] != null) {
                    if (typeof options[interceptor] === 'function') {
                        factoryArgs.push(options[interceptor]);
                        flags |= interceptorFlag[interceptor];
                    } else if (Symbol.iterator in options[interceptor]) {
                        let interceptors = [...options[interceptor]];
                        if (interceptors.length > 1) {
                            for (let i = 0, length = interceptors.length; i < length; ++i) {
                                if (typeof interceptors[i] !== 'function') {
                                    throw new TypeError(`Expected option "${interceptor}" to be a function of iterable of functions, got ${JavaScript.getType(interceptors[i])} at index ${i}`);
                                }
                            }
                            if (interceptor === 'after' || interceptor === 'onCatch') {
                                factoryArgs.push(arrayInterceptorsWithReturn(interceptors));
                            } else {
                                factoryArgs.push(arrayInterceptorsWithoutReturn(interceptors));
                            }
                            flags |= interceptorFlag[interceptor];
                        } else if (interceptors.length > 0) {
                            interceptors = interceptors[0];
                            if (typeof interceptors !== 'function') {
                                throw new TypeError(`Expected option "${interceptor}" to be a function of iterable of functions, got ${JavaScript.getType(interceptors)} at index 0`);
                            }
                            factoryArgs.push(interceptors);
                            flags |= interceptorFlag[interceptor];
                        }
                    } else {
                        throw new TypeError(`Expected option "${interceptor}" to be a function of iterable of functions, got ${JavaScript.getType(options[interceptor])}`);
                    }
                }
            }
            let string = '';
            if (options.name != null) {
                if (typeof options.name !== 'string') {
                    throw new TypeError(`Expected option "name" to be a string, got ${JavaScript.getType(options.name)}`);
                }
                string = factoryOptions.name = options.name;
            }
            const func = getFactory(flags)(...factoryArgs);
            Object.defineProperties(func, {
                toString: {
                    configurable: true,
                    writable: true,
                    value: function () {
                        return `function ${string}() { [native code] }`;
                    }
                },
                name: {
                    configurable: true,
                    value: string
                }
            });
            return func;

            function arrayInterceptorsWithReturn(interceptors) {
                return function (value, entry, platform) {
                    for (const interceptor of interceptors) {
                        value = interceptor(value, entry, platform);
                    }
                    return value;
                };
            }

            function arrayInterceptorsWithoutReturn(interceptors) {
                return function (entry, platform) {
                    for (const interceptor of interceptors) {
                        interceptor(entry, platform);
                    }
                };
            }
        }

        methodFactory(classOptions = {}) {
            classOptions = { ...classOptions };
            for (const interceptor of Object.keys(interceptorFlag)) {
                classOptions[interceptor] = classOptions[interceptor] != null ? Symbol.iterator in classOptions[interceptor] ? [...classOptions[interceptor]] : [classOptions[interceptor]] : [];
            }
            if (classOptions.entry != null) {
                classOptions.entry = { ...classOptions.entry };
            }
            return (implementation, methodOptions = {}) => {
                const generalOptions = { ...classOptions };
                const options = {};
                for (const interceptor of Object.keys(interceptorFlag)) {
                    const methodInterceptors = methodOptions[interceptor] != null ? Symbol.iterator in methodOptions[interceptor] ? [...methodOptions[interceptor]] : [methodOptions[interceptor]] : [];
                    options[interceptor] = [...generalOptions[interceptor], ...methodInterceptors];
                    delete methodOptions[interceptor];
                    delete generalOptions[interceptor];
                }
                if (generalOptions.entry != null || methodOptions.entry != null) {
                    options.entry = { ...generalOptions.entry, ...methodOptions.entry };
                    delete methodOptions.entry;
                    delete generalOptions.entry;
                }
                return this.function(implementation, {
                    ...generalOptions,
                    ...methodOptions,
                    ...options
                });
            };
        }

        markFunctionNative(func) {}

        markFunctionNativeNamed(func, name) {}

        clearFunctionNative(func) {}

        clearFunctionNativeNamed(func) {}

        call(func, ...args) {
            return Function.prototype.call.call(func, ...args);
        }

        apply(func, ...args) {
            return Function.prototype.apply.call(func, ...args);
        }

        bind(func, ...args) {
            return Function.prototype.bind.call(func, ...args);
        }

        is(type) {
            return type in this[symbols.type];
        }

        /**
         * A method to create primordials.
         *
         * Usage of primordials should be avoided in favor of the current context equivalent when possible:
         * For example Object.hasOwnProperty.call(<object>, <property>) will work even if the object arrives from different context;
         * or Object.create(<prototype>, <definition>) will work securely, if <prototype> arrives from different context;
         *
         * When access to the insecure environment is required, primordials can be used:
         * Object.create(platform.primordials['Object.prototype']); // Guarantees the original Object.prototype when if a script overridden it.
         *
         * Note: In this platform implementation, vm contexts are not used, therefore primordials is just a proxy that resolves dot.path from globalThis;
         * A secure implementation must use primordials when necessary to ensure that if the platform is switched to secure one, the implementation remains secure.
         * @protected
         */
        _createPrimordials() {
            return new Proxy(Object.create(null), JavaScript.primordialsGlobalHandler);
        }

        unwrapThis(entry, platform) {
            if (platform.hasImplementation(entry.this)) {
                entry.this = platform.getImplementation(entry.this);
            }
        }

        unwrapOwnThis(entry, platform) {
            if (platform.hasOwnImplementation(entry.this)) {
                entry.this = platform.getOwnImplementation(entry.this);
            }
        }

        unwrapArguments(...indices) {
            return function (entry, platform) {
                for (const index of indices) {
                    if (index >= 0 && index < entry.arguments.length && platform.hasImplementation(entry.arguments[index])) {
                        entry.arguments[index] = platform.getImplementation(entry.arguments[index]);
                    }
                }
            };
        }

        unwrapOwnArguments(...indices) {
            return function (entry, platform) {
                for (const index of indices) {
                    if (index >= 0 && index < entry.arguments.length && platform.hasOwnImplementation(entry.arguments[index])) {
                        entry.arguments[index] = platform.getOwnImplementation(entry.arguments[index]);
                    }
                }
            };
        }

        unwrapAllArguments(entry, platform) {
            for (let index = 0, length = entry.arguments.length; index < length; ++index) {
                if (platform.hasImplementation(entry.arguments[index])) {
                    entry.arguments[index] = platform.getImplementation(entry.arguments[index]);
                }
            }
        }

        unwrapOwnAllArguments(entry, platform) {
            for (let index = 0, length = entry.arguments.length; index < length; ++index) {
                if (platform.hasOwnImplementation(entry.arguments[index])) {
                    entry.arguments[index] = platform.getOwnImplementation(entry.arguments[index]);
                }
            }
        }

        wrapReturnValue(value, entry, platform) {
            if (platform.hasInterface(value)) {
                return platform.getInterface(value);
            }
            return value;
        }

        wrapOwnReturnValue(value, entry, platform) {
            if (platform.hasOwnInterface(value)) {
                return platform.getOwnInterface(value);
            }
            return value;
        }

        get [Symbol.toStringTag]() {
            return 'Platform';
        }
    }

    const interceptorFlag = {
        before: 1,
        after: 2,
        onCatch: 4,
        onFinally: 8
    };

    const allInterceptorFlag = Object.keys(interceptorFlag).reduce((flag, key) => flag | interceptorFlag[key], 0);

    const getFactory = (function () {
        const factories = [];
        for (let i = 1; i <= allInterceptorFlag; ++i) {
            const factory = generateFactoryFunctionCode(i);
            factories[i] = vm.compileFunction(factory.code.join('\n'), factory.args, {
                filename: `platform://dragiyski.org/factory-${factory.interceptors.join('-')}.js`
            });
        }
        factories[0] = function (platform, options, implementation) {
            return function () {
                return implementation.apply(this, arguments);
            };
        };
        return function (elements) {
            if (elements >= 0 && elements < factories.length) {
                return factories[elements];
            }
        };

        function generateFactoryFunctionCode(elements) {
            let code = generateFactoryCode(elements);
            const interceptors = generateFactoryInterceptors(elements);
            code = [
                `return function () {`,
                ...code,
                `}`
            ];
            return {
                code,
                interceptors,
                args: ['platform', 'options', 'implementation', ...interceptors]
            };
        }

        function generateFactoryInterceptors(elements) {
            const interceptors = [];
            for (const interceptorName of ['before', 'after', 'onCatch', 'onFinally']) {
                if (elements & interceptorFlag[interceptorName]) {
                    interceptors.push(interceptorName);
                }
            }
            return interceptors;
        }

        function generateFactoryCode(elements) {
            const hasTry = elements & (interceptorFlag.onCatch | interceptorFlag.onFinally);
            let code = 'entry.this, entry.arguments';
            code = `implementation.apply(${code})`;
            if (elements & interceptorFlag.after) {
                code = `after(${code}, entry, platform)`;
            }
            code = [`${indent(hasTry ? 2 : 1)}return ${code}`];
            if (elements & interceptorFlag.before) {
                code.unshift(`${indent(hasTry ? 2 : 1)}before(entry, platform);`);
            }
            if (hasTry) {
                code.unshift(`${indent(1)}try {`);
                if (elements & interceptorFlag.onCatch) {
                    code.push(
                        `${indent(1)}} catch(exception) {`,
                        `${indent(2)}throw onCatch(exception, entry, platform);`
                    );
                }
                if (elements & interceptorFlag.onFinally) {
                    code.push(
                        `${indent(1)}} finally {`,
                        `${indent(2)}onFinally(entry, platform);`
                    );
                }
                code.push(`${indent(1)}}`);
            }
            code.unshift(
                `${indent(1)}entry = {`,
                `${indent(2)}...options,`,
                `${indent(2)}this: this,`,
                `${indent(2)}arguments: arguments,`,
                `${indent(2)}newTarget: new.target`,
                `${indent(1)}};`
            );
            return code;
        }

        function indent(count) {
            let s = '';
            while (count-- > 0) {
                s += '    ';
            }
            return s;
        }
    })();

    module.exports = Platform;
})();
